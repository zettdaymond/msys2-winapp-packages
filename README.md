# msys2-winapp-packages #

To build a package, run msys2_shell.bat then enter following commands in the bash prompt:
```
$ cd /cloned/repo/dir/some_package
$ makepkg
```

After the package has been build, install it, e.g.:
```
$ pacman -U some_package.pkg.tar.xz
```
